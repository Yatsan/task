package ru.mirea.java.year2020.autumnhome.first;

    class Rectangle extends Shape
    {
        private float width;
        private float height;

        // конструктор с обращением к конструктору класса Shape
        Rectangle(float x, float y, float width, float height){

            super(x,y);
            this.width = width;
            this.height = height;
        }
        @Override
        public float getPerimeter(){

            return width * 2 + height * 2;
        }
        @Override
        public float getArea(){

            return width * height;
        }
    }

