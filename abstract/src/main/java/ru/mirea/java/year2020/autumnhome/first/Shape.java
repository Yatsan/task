package ru.mirea.java.year2020.autumnhome.first;
abstract public class Shape {
    float x; // x-координата точки
    float y; // y-координата точки

    Shape(float x, float y){

        this.x=x;
        this.y=y;
    }

    public float getX() {
        return x;
    }

    public float getY() {
        return y;
    }

    // абстрактный метод для получения периметра
    public abstract float getPerimeter();
    // абстрактный метод для получения площади
    public abstract float getArea();
}



